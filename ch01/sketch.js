class Star {
  constructor() {
    this.x = random(-width, width);
    this.y = random(-height, height);
    this.z = random(0, width);
  }

  update() {
    this.z -= speed;
    if (this.z < 1) {
      this.z = width;
    }
  }

  show() {
    fill(255);
    noStroke();

    const sx = map(this.x / this.z, 0, 1, 0, width);
    const sy = map(this.y / this.z, 0, 1, 0, height);
    const r = map(this.z, 0, width, 16, 0);
    ellipse(sx, sy, r, r);
  }
}

let speed;

const stars = [];

function setup() {
  createCanvas(800, 800);
  for (let i = 0; i < 800; i++) {
    stars.push(new Star());
  }
}

function draw() {
  background(0);
  translate(width / 2, height / 2);
  speed = map(mouseX, 0, width, 0, 20);
  stars.forEach((star) => {
    star.update();
    star.show();
  });
  debugger;

}
